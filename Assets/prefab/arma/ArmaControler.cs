﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmaControler : MonoBehaviour
{
    public GameObject bala;

    public void shoot(float accuracy) {
        Rigidbody2D rbala;
        Vector2 direction;
        Transform trans = gameObject.GetComponentInParent<Transform>();
        float angle, sinAngle, cosAngle;
        Quaternion rotation = trans.rotation;
        GameObject bullet = GameObject.Instantiate(bala, transform.position, rotation);
        angle = (trans.eulerAngles.z + accuracy * 360) * Mathf.PI / 180;    //accuracy es un potencial de desvio
        sinAngle = (float)Mathf.Sin(angle);
        cosAngle = (float)Mathf.Cos(angle); 
        direction = new Vector2(cosAngle, sinAngle);
        rbala = bullet.GetComponent<Rigidbody2D>();
        rbala.velocity = direction.normalized * 50;
    }
    
}
