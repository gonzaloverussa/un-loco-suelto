﻿using UnityEngine;

public class bala : MonoBehaviour {
	float speed = 1.5f;
	
	// Use this for initialization
	void Start () {
		Rigidbody2D rbala;
		Transform trans;
		
		trans = GetComponent<Transform>();
		rbala = GetComponent<Rigidbody2D>();

		Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		Vector2 vect = new Vector2(mousePos.x,mousePos.y);
		vect = vect.normalized;
	}

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag != "Bullet") {
            Destroy(gameObject);
        }
    }
}
