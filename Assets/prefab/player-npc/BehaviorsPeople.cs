﻿using UnityEngine;
using System.Collections;

public class BehaviorsPeople : MonoBehaviour {

	private Rigidbody2D player;
	public float DISTANCE_DETECT = 2f;
	public float DISTANCE_TO_OBSTACLE = 0.75f;
	public float LEADER_BEHIND_DIST = 3f;
	public float LEADER_SIGHT_RADIUS = 1.75f;
	public float SEPARATION_RADIUS = 5f;
	public float MAX_SEPARATION = 1.5f;
	public float SLOWING_RADIUS = 5f;

	void Start () {
		player = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>();
	}

	private Vector2 Seek(Vector2 position, Vector2 velocity, Vector2 positionToSeek, int max_velocity) {
		Vector2 desired_velocity, steering;
		//calcula la velocidad deseada a seguir restando la posicion del jugador menos la de la persona
		desired_velocity = (positionToSeek - position).normalized * max_velocity;
		steering = Vector2.ClampMagnitude(desired_velocity - velocity, max_velocity);
		return Vector2.ClampMagnitude(velocity + steering, max_velocity);
	}

	private Vector2 Flee(Vector2 position, Vector2 velocity, Vector2 positionToFlee, int max_velocity) {
		Vector2 desired_velocity, steering;
		//calcula la velocidad deseada a seguir restando la posicion de la persona menos la del jugador
		desired_velocity = (position - positionToFlee).normalized * max_velocity;
		steering = Vector2.ClampMagnitude(desired_velocity - velocity, max_velocity);
		return Vector2.ClampMagnitude(velocity + steering, max_velocity);
	}

	private Vector2 Arrive(Vector2 position, Vector2 velocity, Vector2 positionToArrive, int max_velocity, float slowingRadius) {
		Vector2 desired_velocity, steering;
		float distance;
		//Calcula la velocidad deseada
		desired_velocity = positionToArrive - position;
		distance = desired_velocity.magnitude;
		desired_velocity = desired_velocity.normalized * max_velocity;
		//Chequea si la distancia es menor al radio, es decir si ya esta dentro del radio
		if (distance < slowingRadius) {
			//reduce la velocidad segun la proximidad
			desired_velocity *= (distance / slowingRadius);
		}
		//Calcula la velocidad que debe tomar
		steering = desired_velocity - velocity;
		return Vector2.ClampMagnitude(velocity + steering, max_velocity);
	}

	private Vector2 Evade(Vector2 position, Vector2 velocity, Vector2 targetPosition, Vector2 targetVelocity, int max_velocity) {
		//algoritmo para evadir la futura posicion de un objetivo en movimiento
		Vector2 distance = targetPosition - position;
		int updatesAhead = (int)distance.magnitude / max_velocity;
		Vector2 futurePosition = targetPosition + targetVelocity.normalized * max_velocity * updatesAhead;
		return Flee(position, velocity, futurePosition, max_velocity);
	}

	public Vector2 SeekPlayer(Collider2D colliderPeople, Vector2 position, Vector2 velocity, int max_velocity) {
		//simple seek del player que evade los obstaculos que tendra en frente al tomar la nueva velocidad que devuelve el seek
		Vector2 seekPlayer = Seek(position, velocity, player.position, max_velocity);
		return AvoidObstacles(colliderPeople, position, seekPlayer, max_velocity);
	}

	public Vector2 FleePlayer(Collider2D colliderPeople, Vector2 position, Vector2 velocity, int max_velocity) {
		//simple flee del player que evade los obstaculos que tendra en frente al tomar la nueva velocidad que devuelve el seek
		Vector2 fleePlayer = Flee(position, velocity, player.position, max_velocity);
		return AvoidObstacles(colliderPeople, position, fleePlayer, max_velocity);
	}

	public Vector2 SeekPlayer2(Collider2D colliderPeople, Vector2 position, Vector2 velocity, int max_velocity) {
		//este seek del player toma en cuenta si con la direccion actual y/o del seekPlayer colisionara con algun obstaculo y
		//segun como se conviene toma diferentes decisiones de movimiento. Gracias a esto no se queda quieto en las esquinas
		Vector2 seekPlayer = Vector2.zero, avoidObstacles1, avoidObstacles2, result = velocity;
		avoidObstacles1 = AvoidObstacles2(colliderPeople, position, velocity, max_velocity);
		seekPlayer = Seek(position, velocity, player.position, max_velocity);
		avoidObstacles2 = AvoidObstacles2(colliderPeople, position, seekPlayer, max_velocity);
		if (avoidObstacles1 == velocity && seekPlayer == avoidObstacles2) {
			//no colisiona adelante ni en la direccion de seekPlayer
			result = seekPlayer;
		}
		if (avoidObstacles1 == velocity && seekPlayer != avoidObstacles2) {
			//colisiona en la direccion del seekPlayer
			result = velocity.normalized * max_velocity * 2 + avoidObstacles2;
		}
		if (avoidObstacles1 != velocity && seekPlayer == avoidObstacles2) {
			//colisiona en la direccion actual
			result = avoidObstacles2;
		}
		if (avoidObstacles1 != velocity && seekPlayer != avoidObstacles2) {
			//colisiona en ambas direcciones
			if (velocity != seekPlayer) {
				Debug.DrawRay(position, Quaternion.AngleAxis(Vector2.Angle(seekPlayer, velocity), Vector3.forward) * velocity);
				result = AvoidObstacles2(colliderPeople, position, Quaternion.AngleAxis(Vector2.Angle(seekPlayer, velocity), Vector3.forward) * velocity, max_velocity);
			}
			else {
				result = avoidObstacles1;
			}
		}
		return Vector2.ClampMagnitude(result, max_velocity);
	}

	public Vector2 FleePlayer2(Collider2D colliderPeople, Vector2 position, Vector2 velocity, int max_velocity) {
		//este flee del player toma en cuenta si con la direccion actual y/o del fleePlayer colisionara con algun obstaculo y
		//segun como se conviene toma diferentes decisiones de movimiento. Gracias a esto no se queda quieto en las esquinas
		Vector2 fleePlayer = Vector2.zero, fleeObstacles1, fleeObstacles2, result = velocity;
		fleeObstacles1 = AvoidObstacles2(colliderPeople, position, velocity, max_velocity);
		fleePlayer = Flee(position, velocity, player.position, max_velocity);
		fleeObstacles2 = AvoidObstacles2(colliderPeople, position, fleePlayer, max_velocity);
		if (fleeObstacles1 == velocity && fleePlayer == fleeObstacles2) {
			//no colisiona adelante ni en la direccion de fleePlayer
			result = fleePlayer;
		}
		if (fleeObstacles1 == velocity && fleePlayer != fleeObstacles2) {
			//colisiona en la direccion del fleePlayer
			result = velocity.normalized * max_velocity * 2 + fleeObstacles2;
		}
		if (fleeObstacles1 != velocity && fleePlayer == fleeObstacles2) {
			//colisiona en la direccion actual
			result = fleeObstacles2;
		}
		if (fleeObstacles1 != velocity && fleePlayer != fleeObstacles2) {
			//colisiona en ambas direcciones
			if (velocity != fleePlayer) {
				Debug.DrawRay(position, Quaternion.AngleAxis(Vector2.Angle(fleePlayer, velocity), Vector3.forward) * velocity);
				result = AvoidObstacles2(colliderPeople, position, Quaternion.AngleAxis(Vector2.Angle(fleePlayer, velocity), Vector3.forward) * velocity, max_velocity);
			}
			else {
				result = fleeObstacles1;
			}
		}
		return Vector2.ClampMagnitude(result, max_velocity);
	}

	private Vector2 AvoidObstacles(Collider2D colliderOrigin, Vector2 position, Vector2 velocity, int max_velocity) {
		//algoritmo para evadir obstaculos que solo utiliza un raycast desde el centro del rigidbody
		Vector2 velocityResult = velocity;
		RaycastHit2D hit;
		colliderOrigin.enabled = false;
		hit = Physics2D.Raycast(position, velocity, DISTANCE_DETECT);
		colliderOrigin.enabled = true;
		if (hit.collider != null && hit.collider.tag == "Obstacle") {
			velocityResult = Seek(position, velocity, hit.point + hit.normal * DISTANCE_TO_OBSTACLE, max_velocity);
		}
		return velocityResult;
	}

	private Vector2 AvoidObstacles2(Collider2D colliderOrigin, Vector2 position, Vector2 velocity, int max_velocity) {
		//algoritmo para evadir obstaculos que utiliza tres raycast, uno desde el centro del rigidbody y otro en cada
		//extremo. Es para que no colisione con los obstaculos en uno de los costados del rigidbody
		Vector2 velocityResult = velocity;
		RaycastHit2D hit1, hit2, hit3;
		colliderOrigin.enabled = false;
		hit1 = Physics2D.Raycast(position + Vector2.Perpendicular(velocity).normalized * 0.45f, velocity, DISTANCE_DETECT);
		hit2 = Physics2D.Raycast(position, velocity, DISTANCE_DETECT);
		hit3 = Physics2D.Raycast(position + Vector2.Perpendicular(velocity).normalized * -0.45f, velocity, DISTANCE_DETECT);
		colliderOrigin.enabled = true;
		if (hit1.collider != null && hit1.collider.tag == "Obstacle") {
			velocityResult = Seek(position, velocity, hit1.point + hit1.normal * DISTANCE_TO_OBSTACLE, max_velocity);
		}
		if (hit3.collider != null && hit3.collider.tag == "Obstacle") {
			velocityResult = Seek(position, velocity, hit3.point + hit3.normal * DISTANCE_TO_OBSTACLE, max_velocity);
		}
		if (hit2.collider != null && hit2.collider.tag == "Obstacle") {
			velocityResult = Seek(position, velocity, hit2.point + hit2.normal * DISTANCE_TO_OBSTACLE, max_velocity);
		}
		return velocityResult;
	}

	private Vector2 separation(Rigidbody2D rbCurrent) {
		//algorimo para separar s los enemigos entre ellos
		Vector2 force = Vector2.zero;
		foreach (var currentGameObject in GameObject.FindGameObjectsWithTag("Enemy")) {
			Rigidbody2D rbEnemy = currentGameObject.GetComponent<Rigidbody2D>();
			if (rbCurrent != rbEnemy && Vector2.Distance(rbEnemy.position, rbCurrent.position) <= SEPARATION_RADIUS) {
				force += rbEnemy.position - rbCurrent.position;
			}
		}
		force *= -1;
		return force.normalized * MAX_SEPARATION;
	}

	public Vector2 followLeader(Rigidbody2D rbCurrent, Rigidbody2D leader, int max_velocity) {
		//algoritmo de seguir al lider sin evadir sus posiciones futuras
		//calcula el vector de su rotacion (frente)
		Vector2 tv = degreeToVector2(leader.rotation), force = Vector2.zero, behind;
		//Calcula el punto detras del lider al que debe ir
		behind = leader.position + tv.normalized * LEADER_BEHIND_DIST * -1;
		//Crea la fuerza para arribar al lider separandose de los demas
		force = Arrive(rbCurrent.position, rbCurrent.velocity, behind, max_velocity, SLOWING_RADIUS);
		force += separation(rbCurrent);
		return force;
	}

	public Vector2 followLeader2(Rigidbody2D rbCurrent, Rigidbody2D leader, int max_velocity) {
		//sigue al lider evadiendo dus posiciones futuras
		Vector2 tv = degreeToVector2(leader.rotation), force = Vector2.zero, ahead, behind;
		//Calcula el punto delante del lider para saber si esta en su camino
		tv = tv.normalized * LEADER_BEHIND_DIST;
		ahead = leader.position + tv;
		//Calcula el punto detras del lider
		behind = leader.position + tv * -1;
		if (isOnLeaderSight(rbCurrent.position, leader, ahead)) {
			//Esta en el camino del lider por lo que hace un evade
			force = Evade(rbCurrent.position, rbCurrent.velocity, leader.position, leader.velocity, max_velocity);
		} else {
			//Agrega fuerza para arrivar a la posicion detras del lider
			force = Arrive(rbCurrent.position, rbCurrent.velocity, behind, max_velocity, SLOWING_RADIUS);
		}
		//agrega fuerza para separarse de los demas
		force += separation(rbCurrent);
		return Vector2.ClampMagnitude(force, max_velocity);
	}

	private bool isOnLeaderSight(Vector2 position, Rigidbody2D leader, Vector2 leaderAhead) {
		//verifica si esta en el camino del lider
		return Vector2.Distance(leaderAhead, position) <= LEADER_SIGHT_RADIUS || Vector2.Distance(leader.position, position) <= LEADER_SIGHT_RADIUS;
	}

	Vector2 degreeToVector2(float angle) {
		//calcula el vector2 que corresponde al angulo
		float sinAngle, cosAngle;
		if (angle < 0) {
			angle = 180 - angle * -1 + 180;
		}
		angle *= Mathf.PI / 180;
		sinAngle = (float)Mathf.Sin(angle);
		cosAngle = (float)Mathf.Cos(angle);
		return new Vector2(cosAngle, sinAngle).normalized;
	}
}