﻿using UnityEngine;

public class Enemy : MonoBehaviour {

	public int max_velocity;
	public Sprite[] cadaver;
	private bool enPersecusion;
	private GameObject puntaje;
	private string leaderName;
	private Rigidbody2D leaderRb;
	private float MAX_DISTANCE_TO_SHOOT;
	private Rigidbody2D rb;
    private Transform trans;
    private float shootCouldown;
    private ArmaControler arma;
    private RaycastHit2D hit;
    private Collider2D colliderEnemy;
    private BehaviorsPeople behaviorsPeople;

    void Start() {
        rb = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        colliderEnemy = GetComponent<Collider2D>();
        behaviorsPeople = GameObject.Find("GameMaster").GetComponent<BehaviorsPeople>();
		puntaje = GameObject.Find("Puntaje");
		rb.freezeRotation = true;
        arma = GetComponentInChildren<ArmaControler>();
        shootCouldown = 2;
		MAX_DISTANCE_TO_SHOOT = 15;
		leaderName = "enemy";   //es uno de los enemy
		if (leaderName != "" && gameObject.name != leaderName && GameObject.Find(leaderName) != null) {
			leaderRb = GameObject.Find(leaderName).GetComponent<Rigidbody2D>();
		}
		enPersecusion = false;
	}

    // Update is called once per frame
    void FixedUpdate() {
        if (enPersecusion){
			if (leaderRb != null) {
				//si el leader esta vivo lo sigue
				rb.velocity = behaviorsPeople.followLeader2(rb, leaderRb, max_velocity);
			} else {
				//si el lider esta muero, busca el player
				rb.velocity = behaviorsPeople.SeekPlayer2(colliderEnemy, rb.position, rb.velocity, max_velocity);
			}
			//se acomoda el sprite para que mire al frente
			mirarAlfrente();
			//habilita y deshabilita el collider porque tenemos un problema y el raycast colisiona con el collider propio
			colliderEnemy.enabled = false;
			//verifica si tiene alguien en el medio
			hit = Physics2D.Raycast(rb.position, rb.velocity, MAX_DISTANCE_TO_SHOOT);
			colliderEnemy.enabled = true;
			if (hit.collider != null && hit.collider.tag == "Player") {
				shootCouldown -= Time.deltaTime;
				if (shootCouldown <= 0) {
					GetComponent<Animator>().SetTrigger("shoot");
					arma.shoot(Random.Range(-0.025f, 0.025f));
					shootCouldown = 2;
				}
			}
			else {
				shootCouldown = 2;
			}
        } else {
            rb.velocity = Vector2.zero;
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Bullet") {
            morir();
        }
    }

    private void morir() {
		Puntaje puntos = puntaje.GetComponent<Puntaje>();
		puntos.sumar(40);


		GameObject cadaver = new GameObject();
        cadaver.transform.localScale = new Vector3(8, 8, 1);
        cadaver.transform.position = rb.position;    //coloca el nuevo render en la posición donde estaba la persona
        cadaver.AddComponent<SpriteRenderer>();
        SpriteRenderer render = cadaver.GetComponent<SpriteRenderer>();
        render.sprite = this.cadaver[Random.Range(0, 15)];
        render.sortingLayerName = "Midground";
        Destroy(gameObject);
    }

    private void mirarAlfrente() {
        float angle = Vector2.Angle(new Vector2(1, 0), rb.velocity);
        float sign = Mathf.Sign(1 * rb.velocity.y - 0 * rb.velocity.x);
        trans.eulerAngles = new Vector3(0, 0, angle * sign);
    }

    public void Alertar() {
        enPersecusion = true;
    }

    public void Tranquilizar() {
        enPersecusion = false;
        rb.velocity = Vector2.zero;
    }
}
