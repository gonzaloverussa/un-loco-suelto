﻿using UnityEngine;

public class People : MonoBehaviour {

	public int max_velocity;
	public Sprite[] cadaver;
	private bool enPanico;
    private GameObject puntaje;
    private Rigidbody2D rb;
    private Transform trans;
    private BehaviorsPeople behaviorsPeople;
    private Collider2D colliderPeople;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        colliderPeople = GetComponent<Collider2D>();
        rb.freezeRotation = true;
        behaviorsPeople = GameObject.Find("GameMaster").GetComponent<BehaviorsPeople>();
		puntaje = GameObject.Find("Puntaje");
		enPanico = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
            Vector2 velocidad = rb.velocity;
		if (enPanico) {
            //Vector2 velocidad = rb.velocity;
            if (velocidad == Vector2.zero) {
                 velocidad = behaviorsPeople.degreeToVector2(rb.rotation) * max_velocity;
            }
            rb.velocity = behaviorsPeople.FleePlayer2(colliderPeople, rb.position, velocidad, max_velocity);
            //se acomoda el sprite para que mire al frente
            mirarAlfrente();
        } else {
            rb.velocity = Vector2.zero;
        }
    }

    

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Bullet") {
            morir();
            
        }
    }

    private void morir() {
		Puntaje puntos = puntaje.GetComponent<Puntaje>();
		puntos.sumar(20);


		GameObject cadaver = new GameObject();
        cadaver.transform.localScale = new Vector3(8,8,1);
        cadaver.transform.position = rb.position;    //coloca el nuevo render en la posición donde estaba la persona
        cadaver.AddComponent<SpriteRenderer>();
        SpriteRenderer render = cadaver.GetComponent<SpriteRenderer>();
        render.sprite = this.cadaver[Random.Range(0,15)];
        render.sortingLayerName = "Midground";
        Destroy(gameObject);
    }

    private void mirarAlfrente() {
        float angle = Vector2.Angle(new Vector2(1, 0), rb.velocity);
        float sign = Mathf.Sign(1 * rb.velocity.y - 0 * rb.velocity.x);
        trans.eulerAngles = new Vector3(0, 0, angle * sign);
    }

    public void Alertar() {
        enPanico = true;
    }

    public void Tranquilizar() {
        enPanico = false;
        rb.velocity = Vector2.zero;
    }
}
