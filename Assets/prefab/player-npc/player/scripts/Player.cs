﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

	public int velocidad;
	private Rigidbody2D rb;
    private Transform trans;
    private Animator anim;
    private bool vivo;
    private ArmaControler arma;
    private int contadorDisparo;

    void Start() {
        contadorDisparo = 15;
        vivo = true;
        rb = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        anim = GetComponent<Animator>();
        arma = GetComponentInChildren<ArmaControler>();
    }

    void OnTriggerEnter2D(Collider2D collider){
        if (collider.tag == "Bullet"){
            //el jugador murió y avisa a los demás
            foreach (var currentGameObject in GameObject.FindGameObjectsWithTag("People")) {
                currentGameObject.SendMessage("Tranquilizar");
            }
            foreach (var currentGameObject in GameObject.FindGameObjectsWithTag("Enemy")) {
                currentGameObject.SendMessage("Tranquilizar");
            }
            morir();
        }
    }

    void morir() {
        anim.SetTrigger("die");
        vivo = false;
        trans.eulerAngles = new Vector3(0, 0, trans.eulerAngles.z - 180);
        //rb.velocity = Vector3.zero;
        GetComponent<SpriteRenderer>().sortingLayerName = "Midground";  //Se pasa al plano Midground para que el cadaver pueda ser pisado
        Destroy(rb);
        Destroy(GetComponent<Collider2D>());
        Destroy(GetComponent<Collider2D>());
        Destroy(GetComponent<CapsuleCollider2D>());
    }

	void FixedUpdate() {
		if (vivo) {

			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			float deltaY = mousePos.y - trans.position.y;
			float deltaX = mousePos.x - trans.position.x;

			float angleInDegrees = Mathf.Atan2(-deltaY, deltaX) * 180 / Mathf.PI;
			trans.eulerAngles = new Vector3(0, 0, -angleInDegrees);

			if (Input.GetKeyDown(KeyCode.E)) {
				//el jugador sacó el arma y alerta a los demás
				foreach (var currentGameObject in GameObject.FindGameObjectsWithTag("People")) {
					currentGameObject.SendMessage("Alertar");
				}
				foreach (var currentGameObject in GameObject.FindGameObjectsWithTag("Enemy")) {
					currentGameObject.SendMessage("Alertar");
				}
				anim.SetTrigger("changeGun");
			}
			if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
				Vector2 vector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
				float inputHorizontal = Input.GetAxis("Horizontal");
				float inputVertical = Input.GetAxis("Vertical");
				rb.velocity = new Vector3(inputHorizontal * velocidad, inputVertical * velocidad, 0.0f);
			}
			else {
				rb.velocity = Vector3.zero;
			}
			if (Input.GetAxis("Fire1") != 0 && anim.GetCurrentAnimatorStateInfo(0).IsName("gun") && contadorDisparo <= 0) {
				contadorDisparo = 15;
				anim.SetTrigger("shoot");
				//arma.shoot(1);
				arma.shoot(Random.Range(-0.015f, 0.015f));
			}
			contadorDisparo -= 1;
		}
		//se hace para probar
		if (Input.GetAxis("Fire2") != 0) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}

    public Animator getAnimator() {
        return anim;
    }

    public Quaternion getRotation() {
        return transform.rotation;
    }

    public float getRotationZ() {
        return transform.eulerAngles.z;
    }

    public Vector3 getPosition() {
        return transform.position;
    }
}
