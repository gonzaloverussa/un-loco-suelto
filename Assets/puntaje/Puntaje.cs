﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Puntaje : MonoBehaviour {
    private Text text;
	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
		text.text = "0";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void sumar(int puntos){
        int suma;

		if (!Int32.TryParse(text.text, out suma)) {
			suma = -1;
		}
		suma = suma + puntos;
		text.text = suma + "";




	}
}
