﻿using UnityEngine;

public class Enemy : MonoBehaviour
{

    public bool enPersecusion;
    public int max_velocity;
    public Sprite[] cadaver;

    private Rigidbody2D rb;
    private Rigidbody2D player;
    private Vector3 rotate;
    private Transform trans;
    private Animator anim;
    private float couldown;
    private ArmaControler arma;
    private RaycastHit2D hit;
    private RaycastHit2D[] hits;
    private Collider2D collider;

    // Use this for initialization
    void Start(){
        rb = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        collider = GetComponent<Collider2D>();
        player = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>();
        rb.freezeRotation = true;
        anim = GetComponent<Animator>();
        arma = GetComponentInChildren<ArmaControler>();
        couldown = 2;
    }

    // Update is called once per frame
    void FixedUpdate(){
        //if (player)
        Vector2 desired_velocity, steering, direction;
        if (enPersecusion){
            //para calcular la velocidad deseada para huir se resta la posicion de la persona menos la del jugador
            direction = (player.position - rb.position).normalized;
            desired_velocity = direction * max_velocity;

            /*   if (rb.velocity == Vector2.zero){
                   //si está quieto se le agrega un movimiento hacia adelante para que no salga caminando hacia atras
                   rb.velocity = transform.forward;
               }*/
            steering = Vector3.ClampMagnitude(desired_velocity - rb.velocity, max_velocity);


            rb.velocity = Vector3.ClampMagnitude(rb.velocity + steering, max_velocity);

            //se acomoda el sprite para que mire al frente
            mirarAlfrente();

            //habilita y deshabilita el collider porque tenemos un problema y el raycast colisiona con el collider propio
            collider.enabled = false;
            //verifica si tiene alguien en el medio
            hit = Physics2D.Raycast(rb.position, direction);
            collider.enabled = true;
            if (hit.collider.tag == "Player") {
                couldown -= Time.deltaTime;
                if (couldown <= 0) {
                    print("shoot");
                    arma.shoot(Random.Range(0.9f, 1.1f));
                    couldown = 2;
                }
            } else {
                couldown = 2;
            }
            
        } else {
            rb.velocity = Vector2.zero;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Bullet")
        {
            morir();
        }
    }

    private void morir()
    {
        GameObject cadaver = new GameObject();
        cadaver.transform.localScale = new Vector3(8, 8, 1);
        cadaver.transform.position = rb.position;    //coloca el nuevo render en la posición donde estaba la persona
        cadaver.AddComponent<SpriteRenderer>();
        SpriteRenderer render = cadaver.GetComponent<SpriteRenderer>();
        render.sprite = this.cadaver[Random.Range(0, 15)];
        render.sortingLayerName = "Midground";
        Destroy(gameObject);
    }

    private void mirarAlfrente()
    {
        float angle = Vector2.Angle(new Vector2(1, 0), rb.velocity);
        float sign = Mathf.Sign(1 * rb.velocity.y - 0 * rb.velocity.x);
        trans.eulerAngles = new Vector3(0, 0, angle * sign);
    }

    public void alertar(){
        enPersecusion = true;
    }

    public void tranquilizar() {
        enPersecusion = false;
        rb.velocity = Vector2.zero;
    }
}
