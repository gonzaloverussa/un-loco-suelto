﻿using UnityEngine;

public class People : MonoBehaviour {

    public bool enPanico;
    public int max_velocity;
    public Sprite[] cadaver;

    private Rigidbody2D rb;
    private Rigidbody2D player;
    private Vector3 rotate;
    private Transform trans;
    private Animator anim;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        player = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>();
        rb.freezeRotation = true;
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        Vector2 desired_velocity, steering;
        if (enPanico) {
            //para calcular la velocidad deseada para huir se resta la posicion de la persona menos la del jugador
            desired_velocity = (rb.position - player.position).normalized * max_velocity;

            /*   if (rb.velocity == Vector2.zero){
                   //si está quieto se le agrega un movimiento hacia adelante para que no salga caminando hacia atras
                   rb.velocity = transform.forward;
               }*/
            steering = Vector3.ClampMagnitude(desired_velocity - rb.velocity, max_velocity);


            rb.velocity = Vector3.ClampMagnitude(rb.velocity + steering, max_velocity);

            //se acomoda el sprite para que mire al frente
            mirarAlfrente();
        } else {
            rb.velocity = Vector2.zero;
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Bullet") {
            morir();
        }
    }

    private void morir() {
        GameObject cadaver = new GameObject();
        cadaver.transform.localScale = new Vector3(8,8,1);
        cadaver.transform.position = rb.position;    //coloca el nuevo render en la posición donde estaba la persona
        cadaver.AddComponent<SpriteRenderer>();
        SpriteRenderer render = cadaver.GetComponent<SpriteRenderer>();
        render.sprite = this.cadaver[Random.Range(0,15)];
        render.sortingLayerName = "Midground";
        Destroy(gameObject);
    }

    private void mirarAlfrente() {
        float angle = Vector2.Angle(new Vector2(1, 0), rb.velocity);
        float sign = Mathf.Sign(1 * rb.velocity.y - 0 * rb.velocity.x);
        trans.eulerAngles = new Vector3(0, 0, angle * sign);
    }

    public void alertar() {
        enPanico = true;
    }

    public void tranquilizar() {
        enPanico = false;
        rb.velocity = Vector2.zero;
    }
}
