﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmaControler : MonoBehaviour {
    public GameObject player;
    private Vector3 offset;
    public GameObject bala;
    private player playerScript;
   

    // Use this for initialization
    void Start()
    {
        playerScript = player.GetComponent<player>();
       // offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
      //  transform.position = player.transform.position + offset;
    }

    private void FixedUpdate()
    {

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float time = Time.deltaTime;
        if (Input.GetAxis("Fire1") != 0 && playerScript.getAnimator().GetCurrentAnimatorStateInfo(0).IsName("nada") )
        {
            //Instantiate (bala, transform.position, Quaternion.identity);
            Rigidbody2D rbala;
            Vector3 heading;
            float distancia;
            heading = playerScript.getPosition() - mousePos;
            distancia = heading.magnitude;
            Debug.Log(distancia);
            if (distancia > 10.0629) { 
                GameObject bullet = GameObject.Instantiate(bala, transform.position, playerScript.getRotation()) as GameObject;
                playerScript.getAnimator().SetTrigger("shoot");
                Vector2 vect = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);
                rbala = bullet.GetComponent<Rigidbody2D>();
                vect = vect.normalized;
                rbala.velocity = vect * 40;
            }
        }
    }
}
