﻿using UnityEngine;

public class bala : MonoBehaviour {
	float speed = 1.5f;
    private Rigidbody2D rb;
    private Collider2D collider;
    private Transform trans;
    private Vector2 previousPosition;
    private float partialExtent;
    private float minimumExtent;
    private float sqrMinimumExtent;
    private LayerMask layerMask = -1; //make sure we aren't in this layer 
    private float skinWidth = 0.1f; //probably doesn't need to be changed 

    // Use this for initialization
    void Start () {
		
		trans = GetComponent<Transform>();
		rb = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		Vector2 vect = new Vector2(mousePos.x,mousePos.y);
		vect = vect.normalized;


        //para que la bala no traspase las paredes
        previousPosition = rb.position;
        minimumExtent = Mathf.Min(Mathf.Min(collider.bounds.extents.x, collider.bounds.extents.y), collider.bounds.extents.z);
        partialExtent = minimumExtent * (1.0f - skinWidth);
        sqrMinimumExtent = minimumExtent * minimumExtent;

    }

    //private void FixedUpdate() {
    //    Vector3 movementThisStep = rb.position - previousPosition;
    //    float movementSqrMagnitude = movementThisStep.sqrMagnitude;
    //    if (movementSqrMagnitude > sqrMinimumExtent) {
    //        float movementMagnitude = Mathf.Sqrt(movementSqrMagnitude);
    //        RaycastHit hitInfo;

    //        //check for obstructions we might have missed 
    //        if (Physics.Raycast(previousPosition, movementThisStep, out hitInfo, movementMagnitude, layerMask.value)) {
    //            if (hitInfo.collider) {

    //                if (hitInfo.collider.isTrigger)
    //                    hitInfo.collider.SendMessage("OnTriggerEnter2D", collider);

    //                if (!hitInfo.collider.isTrigger)
    //                    rb.position = hitInfo.point - (movementThisStep / movementMagnitude) * partialExtent;
    //            }
    //        }
    //    }
    //    previousPosition = rb.position;
    //}

    void OnTriggerEnter2D(Collider2D collider) {
        Destroy(gameObject);
    }
}
