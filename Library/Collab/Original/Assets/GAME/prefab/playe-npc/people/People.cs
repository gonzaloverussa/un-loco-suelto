﻿using UnityEngine;

public class People : MonoBehaviour {

    public bool enPanico;
    public int max_velocity;
    public Sprite[] cadaver;

    private Rigidbody2D rb;
    private Vector3 rotate;
    private Transform trans;
    private Animator anim;
    private BehaviorsPeople behaviorsPeople;
    private Collider2D colliderPeople;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        colliderPeople = GetComponent<Collider2D>();
        rb.freezeRotation = true;
        anim = GetComponent<Animator>();
        behaviorsPeople = GameObject.Find("GameMaster").GetComponent<BehaviorsPeople>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (enPanico) {
            Vector2 velocidad = rb.velocity;
            if (velocidad == Vector2.zero) {
                 velocidad = trans.rotation * Vector2.up;
            }
            print(velocidad);
            //Debug.DrawRay(rb.position, velocidad);
            //Vector2 avoidVelocity = behaviorsPeople.FleePlayer(rb.position, rb.velocity, max_velocity);
            rb.velocity = behaviorsPeople.FleePlayer(colliderPeople, rb.position, velocidad, max_velocity);
            //Vector2 velocidad =  behaviorsPeople.AvoidObstacles(colliderPeople, rb.position, rb.velocity, max_velocity);
            //print("avoid velocity " + velocidad);
            
            //rb.velocity = (avoidVelocity + velocidad).normalized * max_velocity;
            //rb.velocity = behaviorsPeople.FleePlayer(rb.position, rb.velocity, max_velocity);

            //se acomoda el sprite para que mire al frente
            mirarAlfrente();
        } else {
            rb.velocity = Vector2.zero;
        }
    }

    

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Bullet") {
            morir();
        }
    }

    private void morir() {
        GameObject cadaver = new GameObject();
        cadaver.transform.localScale = new Vector3(8,8,1);
        cadaver.transform.position = rb.position;    //coloca el nuevo render en la posición donde estaba la persona
        cadaver.AddComponent<SpriteRenderer>();
        SpriteRenderer render = cadaver.GetComponent<SpriteRenderer>();
        render.sprite = this.cadaver[Random.Range(0,15)];
        render.sortingLayerName = "Midground";
        Destroy(gameObject);
    }

    private void mirarAlfrente() {
        float angle = Vector2.Angle(new Vector2(1, 0), rb.velocity);
        float sign = Mathf.Sign(1 * rb.velocity.y - 0 * rb.velocity.x);
        trans.eulerAngles = new Vector3(0, 0, angle * sign);
    }

    public void alertar() {
        enPanico = true;
    }

    public void tranquilizar() {
        enPanico = false;
        rb.velocity = Vector2.zero;
    }
}
