﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {
    private Rigidbody2D rb;
    public int fuerza;
    public int velocidad;
    private Transform trans;
	private Animator anim;
	public GameObject bala;
	public bool vivo;

    

	// Use this for initialization
	void Start () {
		vivo = true;
		rb = GetComponent<Rigidbody2D>();
		trans = GetComponent<Transform>();
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (vivo){
            if (Input.GetAxis("Jump") !=0 ){
				morir();
				
			}
		}
		
	}
	
	void morir(){
		anim.SetTrigger("die");
		vivo = false;
		trans.eulerAngles = new Vector3 (0, 0, trans.eulerAngles.z - 180);
		rb.velocity = Vector3.zero;
		//rb.bodyType =  RigidbodyType2D.Kinematic;
	}

	void FixedUpdate () {
        if (vivo){
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            float deltaY = mousePos.y - trans.position.y;
            float deltaX = mousePos.x - trans.position.x;
            float angleInDegrees = Mathf.Atan2(-deltaY, deltaX) * 180 / Mathf.PI;
            trans.eulerAngles = new Vector3(0, 0, -angleInDegrees);
   //         if (Input.GetAxis("Fire1") !=0 && anim.GetCurrentAnimatorStateInfo(0).IsName("nada")){
			//	//anim.SetTrigger("shoot");
			//	//Instantiate (bala, transform.position, Quaternion.identity);
			///*	Rigidbody2D rbala;
			//	GameObject bullet=GameObject.Instantiate(bala, transform.position, transform.rotation) as GameObject;

			//	Vector2 vect = new Vector2(mousePos.x -transform.position.x , mousePos.y - transform.position .y );

			//	rbala = bullet.GetComponent<Rigidbody2D>();
			//	vect = vect.normalized;
			//	rbala.velocity= vect *70;*/
			//}


            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            {
                Vector2 vector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
                float inputHorizontal = Input.GetAxis("Horizontal");
                float inputVertical = Input.GetAxis("Vertical");

                Vector3 newVelocity = new Vector3(inputHorizontal * velocidad, inputVertical * velocidad, 0.0f);
                rb.velocity = newVelocity;
            }
            else
            {
                rb.velocity = Vector3.zero;
            }

        }
	}

    public Animator getAnimator()
    {
        return anim;
    
    }

    public Quaternion getRotation()
    {
        return transform.rotation;
    }

    public Vector3 getPosition()
    {
       return transform.position;
    }
}
